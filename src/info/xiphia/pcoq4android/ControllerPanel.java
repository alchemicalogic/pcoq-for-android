package info.xiphia.pcoq4android;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

/**
 * Created with IntelliJ IDEA.
 * User: xiphia
 * Date: 13/09/10
 * Time: 2:33
 * To change this template use File | Settings | File Templates.
 */
public class ControllerPanel extends Fragment implements View.OnClickListener {
    ListView memberList;
    ArrayAdapter<String> adapter;
    EditText editAnswer = null;
    Button buttonAnswer = null;
    Button buttonSay = null;
    Button buttonTextAnswer = null;
    Button buttonSecret = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        adapter = new ArrayAdapter<String>(getActivity(), R.layout.member_list_item, R.id.memberTextView);
        return inflater.inflate(R.layout.controller, null);
    }

    @Override
    public void onStart() {
        super.onStart();
        editAnswer = (EditText)getActivity().findViewById(R.id.editAnswer);
        buttonAnswer = (Button)getActivity().findViewById(R.id.buttonAnswer);
        buttonAnswer.setOnClickListener(this);
        buttonSay = (Button)getActivity().findViewById(R.id.buttonSay);
        buttonSay.setOnClickListener(this);
        buttonTextAnswer = (Button)getActivity().findViewById(R.id.buttonTextAnswer);
        buttonTextAnswer.setOnClickListener(this);
        buttonSecret = (Button)getActivity().findViewById(R.id.buttonSecret);
        buttonSecret.setOnClickListener(this);
        memberList = (ListView)getActivity().findViewById(R.id.memberList);
        memberList.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        MainActivity mainActivity = (MainActivity)getActivity();
        switch(view.getId()) {
            case R.id.buttonAnswer:
                //LogPanel logPanel = (LogPanel)((ViewPager)getActivity().findViewById(R.id.viewPager)).getAdapter().instantiateItem((ViewPager)getActivity().findViewById(R.id.viewPager), 1);
                //logPanel.addLog("Answered!");
                mainActivity.send("say !");
                editAnswer.setText("");
                break;
            case R.id.buttonSay:
                mainActivity.send("say " + editAnswer.getText().toString());
                editAnswer.setText("");
                break;
            case R.id.buttonTextAnswer:
                mainActivity.send("board " + editAnswer.getText().toString());
                editAnswer.setText("");
                break;
            case R.id.buttonSecret:
                mainActivity.send("say *" + editAnswer.getText().toString());
                editAnswer.setText("");
                break;
        }
    }

    public void addMember(String str) {
        adapter.add(str);
    }

    public void clearMember() {
        adapter.clear();
    }
}
