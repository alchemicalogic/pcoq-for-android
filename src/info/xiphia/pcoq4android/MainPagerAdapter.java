package info.xiphia.pcoq4android;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created with IntelliJ IDEA.
 * User: xiphia
 * Date: 2013/09/08
 * Time: 10:58
 * To change this template use File | Settings | File Templates.
 */
public class MainPagerAdapter extends FragmentStatePagerAdapter {
    static final int NUM_VIEW = 3;

    public MainPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int i) {
        switch(i) {
            case 0:
                return new ControllerPanel();
            case 1:
                return  new LogPanel();
            default:
                return new MiscPanel();
        }
    }

    @Override
    public int getCount() {
        return NUM_VIEW;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(position == 0) {
            return "コントローラー";
        } else if(position == 1) {
            return "ログ";
        } else {
            return "その他";
        }
    }
}
