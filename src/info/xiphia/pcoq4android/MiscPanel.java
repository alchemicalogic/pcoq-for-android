package info.xiphia.pcoq4android;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created with IntelliJ IDEA.
 * User: xiphia
 * Date: 13/09/10
 * Time: 2:40
 * To change this template use File | Settings | File Templates.
 */
public class MiscPanel extends Fragment implements View.OnClickListener {
    Button buttonLogout = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.misc, null);
    }

    @Override
    public void onStart() {
        super.onStart();
        buttonLogout = (Button)getActivity().findViewById(R.id.buttonLogout);
        buttonLogout.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        MainActivity mainActivity = (MainActivity)getActivity();
        switch(view.getId()) {
            case R.id.buttonLogout:
                mainActivity.disconnect();
                break;
        }
    }
}
