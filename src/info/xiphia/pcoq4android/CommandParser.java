package info.xiphia.pcoq4android;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: xiphia
 * Date: 13/09/10
 * Time: 11:34
 * To change this template use File | Settings | File Templates.
 */
public class CommandParser {
    public CommandParser() {
    }

    public Command parse(String str) {
        ArrayList<String> parameters = new ArrayList<String>();
        ArrayList<String> options = new ArrayList<String>();
        String temp;
        if(str.startsWith("msg")) {
            if(str.matches(".*/0x[0-9a-f]{6}$"))  {
                parameters.add(str.substring(str.indexOf(" ") + 1, str.lastIndexOf("/")));
                options.add(str.substring(str.lastIndexOf("/") + 1));
            } else {
                parameters.add(str.substring(str.indexOf(" ") + 1));
            }
            return new Command("msg", parameters, options);
        } else if(str.startsWith("pts")) {
            for(String param: str.substring(str.indexOf(" ") + 1).split(",")) {
                parameters.add(param);
            }
            return new Command("pts", parameters, options);
        } else if(str.startsWith("join")) {
            for(String param: str.substring(str.indexOf(" ") + 1).split(",")) {
                parameters.add(param);
            }
            return new Command("join", parameters, options);
        } else if(str.startsWith("exinfo")) {
            for(String param: str.substring(str.indexOf(" ") + 1).split(",")) {
                parameters.add(param);
            }
            return new Command("exinfo", parameters, options);
        } else if(str.startsWith("push")) {
            parameters.add(str.substring(str.indexOf(" ") + 1));
            return new Command("push", parameters, options);
        } else if(str.startsWith("sound")) {
            parameters.add(str.substring(str.indexOf(" ") + 1));
            return new Command("sound", parameters, options);
        } else if(str.startsWith("version")) {
            parameters.add(str.substring(str.indexOf(" ") + 1));
            return new Command("version", parameters, options);
        } else if(str.startsWith("yourID")) {
            parameters.add(str.substring(str.indexOf(" ") + 1));
            return new Command("yourID", parameters, options);
        } else if(str.equals("nextQ")) {
            return new Command(str, parameters, options);
        } else if(str.equals("clear")) {
            return new Command(str, parameters, options);
        } else if(str.equals("display")) {
            return new Command(str, parameters, options);
        } else if(str.equals("reset")) {
            return new Command(str, parameters, options);
        } else if(str.equals("update")) {
            return new Command(str, parameters, options);
        } else if(str.equals("allclear")) {
            return new Command(str, parameters, options);
        } else {
            return null;
        }
    }
}
