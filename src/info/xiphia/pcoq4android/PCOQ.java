package info.xiphia.pcoq4android;

import android.content.Context;
import android.view.View;
import android.support.v4.view.ViewPager;
import android.util.Log;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: xiphia
 * Date: 13/09/10
 * Time: 3:41
 * To change this template use File | Settings | File Templates.
 */
public class PCOQ {
    final static String PROTOCOL_VERSION = "1.1";
    final static int PCOQ_PORT = 52294;
    private Socket pcoqSocket = null;
    private BufferedWriter bufferedWriter;
    private BufferedReader bufferedReader;
    private Thread receiveThread;
    private boolean receiveFlag;
    private ArrayList<String> logData;
    private ArrayList<String> participant;
    private ArrayList<Integer> answerOrder;
    private Context context;
    private CommandParser parser;
    private int myID;

    public PCOQ(Context context) {
        this.context = context;
        this.answerOrder = new ArrayList<Integer>();
        this.participant = new ArrayList<String>();
    }

    public void connect(String host, String name) {
        try {
            pcoqSocket = new Socket(host, PCOQ_PORT);
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(pcoqSocket.getOutputStream(), "Shift_JIS"));
            bufferedReader = new BufferedReader(new InputStreamReader(pcoqSocket.getInputStream(), "Shift_JIS"));
            for(int i = 0; i < 25; i++) {
                participant.add(i, "");
            }
            send("match " + name);
        } catch(Exception e) {
            e.printStackTrace();
        }
        receiveFlag = true;
        receive();
    }

    public void send(String str) {
        try {
            bufferedWriter.write(str + "\n");
            bufferedWriter.flush();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private void receive() {
        receiveThread = new Thread(new Runnable() {
            String str;
            @Override
            public void run() {
                parser = new CommandParser();
                while(receiveFlag) {
                    try {
                        str = bufferedReader.readLine();
                        if(str != null) {
                            Log.d("PCOQ4Android", str);
                            final Command command = parser.parse(str);
                            Log.d("PCOQ4Android", command.toString());
                            if(command.getCommand().equals("nextQ")) {
                                ((MainActivity)context).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ((MainActivity) context).showToast("次の問題");
                                    }
                                });
                                answerOrder.clear();
                                ((MainActivity)context).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ControllerPanel controllerPanel = (ControllerPanel)((ViewPager)((MainActivity)context).findViewById(R.id.viewPager)).getAdapter().instantiateItem((ViewPager)((MainActivity)context).findViewById(R.id.viewPager), 0);
                                        controllerPanel.clearMember();
                                    }
                                });
                            } else if(command.getCommand().equals("reset") || command.getCommand().equals("allclear")) {
                                answerOrder.clear();
                                ((MainActivity)context).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ControllerPanel controllerPanel = (ControllerPanel)((ViewPager)((MainActivity)context).findViewById(R.id.viewPager)).getAdapter().instantiateItem((ViewPager)((MainActivity)context).findViewById(R.id.viewPager), 0);
                                        controllerPanel.clearMember();
                                    }
                                });
                            } else if(command.getCommand().equals("yourID")) {
                                myID = Integer.valueOf(command.getParameters().get(0));
                            } else if(command.getCommand().equals("push")) {
                                answerOrder.add(Integer.valueOf(command.getParameters().get(0)));
                                ((MainActivity)context).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ControllerPanel controllerPanel = (ControllerPanel)((ViewPager)((MainActivity)context).findViewById(R.id.viewPager)).getAdapter().instantiateItem((ViewPager)((MainActivity)context).findViewById(R.id.viewPager), 0);
                                        controllerPanel.addMember(participant.get(Integer.valueOf(command.getParameters().get(0))));
                                    }
                                });
                                for(String nm: participant) {
                                   Log.d("PCOQ4Android", nm);
                                }
                                if(answerOrder.get(0) == myID) {
                                    ((MainActivity)context).runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            ((MainActivity) context).showToast("回答権を得ました!");
                                        }
                                    });
                                } else {
                                    ((MainActivity)context).runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            ((MainActivity) context).showToast(participant.get(answerOrder.get(0)) + "さんが回答権を得ました");
                                        }
                                    });
                                }
                            } else if(command.getCommand().equals("join")) {
                                participant.set(Integer.valueOf(command.getParameters().get(0)), command.getParameters().get(1));
                                Log.d("PCOQ4Android", Integer.valueOf(command.getParameters().get(0)).toString() + ":" + command.getParameters().get(1));
                            } else if(command.getCommand().equals("msg")) {
                                ((MainActivity)context).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ((MainActivity)context).showToast(command.getParameters().get(0));
                                        LogPanel logPanel = (LogPanel)((ViewPager)((MainActivity)context).findViewById(R.id.viewPager)).getAdapter().instantiateItem((ViewPager)((MainActivity)context).findViewById(R.id.viewPager), 1);
                                        logPanel.addLog(command.getParameters().get(0));
                                    }
                                });
                            }
                            logData.add(str);
                        }
                    } catch(Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        receiveThread.start();
    }

    public void disconnect() {
        send("leave");
        participant.clear();
        answerOrder.clear();
        receiveFlag = false;
        try {
            pcoqSocket.close();
            pcoqSocket = null;
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isConnected() {
        return (pcoqSocket != null) && (pcoqSocket.isConnected());
    }

    public ArrayList<String> getLogData() {
        return logData;
    }
}
