package info.xiphia.pcoq4android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created with IntelliJ IDEA.
 * User: xiphia
 * Date: 2013/09/08
 * Time: 6:06
 * To change this template use File | Settings | File Templates.
 */
public class LoginActivity extends Activity implements View.OnClickListener {
    EditText editServer = null;
    EditText editName = null;
    Button loginButton = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        editServer = (EditText)findViewById(R.id.editServer);
        editName = (EditText)findViewById(R.id.editName);
        loginButton = (Button)findViewById(R.id.buttonLogin);
        loginButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == loginButton.getId()) {
            Intent data = new Intent();
            Bundle bundle = new Bundle();
            bundle.putString("host", editServer.getText().toString());
            bundle.putString("name", editName.getText().toString());
            data.putExtras(bundle);
            setResult(RESULT_OK, data);
            finish();
        }
    }
}