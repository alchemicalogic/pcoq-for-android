package info.xiphia.pcoq4android;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.PagerAdapter;
import android.widget.Toast;

/**
 * Created with IntelliJ IDEA.
 * User: xiphia
 * Date: 2013/09/08
 * Time: 7:05
 * To change this template use File | Settings | File Templates.
 */
public class MainActivity extends FragmentActivity {
    private ViewPager mViewPager;
    private PagerAdapter mPagerAdapter;
    protected PCOQ pcoq;
    private Intent loginIntent = null;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        pcoq = new PCOQ(this);

        mViewPager = (ViewPager)findViewById(R.id.viewPager);
        mPagerAdapter = new MainPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);

        showLoginIntent();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        disconnect();
    }

    private void showLoginIntent() {
        loginIntent = new Intent(MainActivity.this, LoginActivity.class);
        startActivityForResult(loginIntent, 0x01);
    }

    public void send(String str) {
        new CommandTask().execute(str);
    }

    public void disconnect() {
        new LogoutTask(this).execute();
        loginIntent = new Intent(MainActivity.this, LoginActivity.class);
        startActivityForResult(loginIntent, 0x01);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case 0x01:
                if(resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    new LoginTask(this).execute(bundle.getString("host"), bundle.getString("name"));
                }
                break;
        }
    }

    private class LoginTask extends AsyncTask<String, Void, Boolean> {
        Context context;
        ProgressDialog dialog;

        public LoginTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(context);
            dialog.setTitle("接続中");
            dialog.setTitle("PCOQサーバーへ接続中");
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.show();
        }

        @Override
        protected Boolean doInBackground(String[] params) {
            pcoq.connect(params[0], params[1]);
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            dialog.dismiss();
        }
    }

    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    private class LogoutTask extends AsyncTask<String, Void, Boolean> {
        Context context;
        ProgressDialog dialog;

        public LogoutTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(context);
            dialog.setTitle("切断中");
            dialog.setTitle("PCOQサーバーから切断中");
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.show();
        }

        @Override
        protected Boolean doInBackground(String[] params) {
            pcoq.disconnect();
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            dialog.dismiss();
        }
    }

    private class CommandTask extends AsyncTask<String, Void, Boolean> {
        @Override
        protected Boolean doInBackground(String[] params) {
            pcoq.send(params[0]);
            return true;
        }
    }
}