package info.xiphia.pcoq4android;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: xiphia
 * Date: 13/09/10
 * Time: 11:40
 * To change this template use File | Settings | File Templates.
 */
public class Command {
    private String command;
    private ArrayList<String> parameters;
    private ArrayList<String> options;

    public Command(String command, ArrayList<String> parameters, ArrayList<String> options) {
        this.command = command;
        this.parameters = parameters;
        this.options = options;
    }

    public String getCommand() {
        return this.command;
    }

    public ArrayList<String> getParameters() {
        return this.parameters;
    }

    public ArrayList<String> getOptions() {
        return this.options;
    }

    public String toString() {
        String result;
        result = command;
        if(parameters.isEmpty() == false) {
            result += " ";
            for(String param: parameters) {
                result += param + ",";
            }
        }
        return result;
    }
}
