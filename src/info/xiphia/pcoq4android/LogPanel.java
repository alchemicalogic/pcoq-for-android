package info.xiphia.pcoq4android;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created with IntelliJ IDEA.
 * User: xiphia
 * Date: 13/09/10
 * Time: 2:39
 * To change this template use File | Settings | File Templates.
 */
public class LogPanel extends Fragment {
    ListView logList;
    ArrayAdapter<String> adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        adapter = new ArrayAdapter<String>(getActivity(), R.layout.log_list_item, R.id.logTextView);
        return inflater.inflate(R.layout.log, null);
    }

    @Override
    public void onStart() {
        super.onStart();
        logList = (ListView)getActivity().findViewById(R.id.logList);
        logList.setAdapter(adapter);
    }

    public void addLog(String str) {
        adapter.add(str);
    }

    public void clearLog() {
        adapter.clear();
    }
}
